//APPNAME	:=	task1.50.cpp

#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

int main()
{
	setlocale(LC_CTYPE,"rus");
	std::cout << "Введите координаты вершин треугольника:" << std::endl;
	float x1,x2,x3,y1,y2,y3,s,p,pp,a,b,c;
	cout << "X1 = ";
	cin >> x1;
	cout << "Y1 = ";
	cin >> y1;
	cout << "X2 = ";
	cin >> x2;
	cout << "Y2 = ";
	cin >> y2;
	cout << "X3 = ";
	cin >> x3;
	cout << "Y3 = ";
	cin >> y3;
	a = sqrt(((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)));
	cout << "A = " << a << endl;
	b = sqrt(((x1-x3)*(x1-x3))+((y1-y3)*(y1-y3)));
	cout << "B = " << b << endl;
	c = sqrt(((x2-x3)*(x2-x3))+((y2-y3)*(y2-y3)));
	cout << "C = " << c << endl;
	p = a + b + c;
	cout << "Периметр (P) = " << p << endl;
	pp = p/2;
	s = sqrt(pp*(pp-a)*(pp- b )*(pp-c));
	cout << "Площадь (S) = " << s << endl;
	system("pause");
	return 0;
}
/* TASK #	:=	1.50
 * EDITED 	:=	05/02/2019 */
