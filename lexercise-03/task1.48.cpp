//APPNAME	:=	task1.48.cpp

#include <iostream>
#include <stdlib.h>
#include <math.h>

int main()
{
	setlocale(LC_CTYPE,"rus");
	float h,a,b,c,d;
	std::cout << "Введите значение высоты трапеции:" << std::endl;
	std::cin >> h;
	std::cout << "Введите значение большего основания:" << std::endl;
	std::cin >> a;
	std::cout << "Введите значение меньшего основания:" << std::endl;
	std::cin >> b;
	c = (h-a)/2;
	d = sqrt(pow(c, 2)+pow(b, 2));
	std::cout << "Периметр (P) = " << h + a + 2*d << std::endl;
	system("pause");
	return 0;
}
/* TASK #	:=	1.48
 * EDITED 	:=	04/02/2019 */
