//APPNAME	:=	task1.38-a.cpp

#include <iostream>
#include <stdlib.h>
#include <math.h>

int main()
{
    setlocale(LC_CTYPE,"rus");
    int x,y;
    double z;
    std::cout << "Введите любое значение X и Y: " << std::endl;
    std::cin >> x >> y;
    z = pow(x, 3)-(2.5*x*y)+(1.78*pow(x, 2))-2.5*y+1;
    std::cout << "Ответ = " << z << std::endl;
    system("pause");
    return 0;
}
/* TASK #	:=	1.38-A
 * EDITED	:=	04/02/2019 */
