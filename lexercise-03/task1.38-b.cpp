//APPNAME	:=	task1.38-b.cpp

#include <iostream>
#include <stdlib.h>
#include <math.h>

int main()
{
    setlocale(LC_CTYPE,"rus");
    int a,b;
    double x;
    std::cout << "Введите любое значение A и B: " << std::endl;
    std::cin >> a >> b;
    x = (3.56*a*pow(b, 3))-(5.8*pow(b, 2))+3.8*a-1.5;
    std::cout << "Ответ = " << x << std::endl;
    system("pause");
    return 0;
}
/* TASK #	:=	1.38-B
 * EDITED	:=	04/02/2019 */
