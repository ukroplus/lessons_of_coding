// lexercise1-02.cpp:

#include <iostream>
#include <stdlib.h>

int main()
{
	setlocale(LC_CTYPE, "rus");
	int N;
	std::cout << "Введите время в секундах = ";
	std::cin >> N;
	std::cout << "час:мин:сек" << std::endl;
	std::cout << N/3600;
	std::cout << ":";
	std::cout << N/60;
	std::cout << ":";
	std::cout << N%60 << std::endl;
	system("pause");
	return 0;
}
	/* Wed 30 Jan 2019 */
