// lexercise1-02.cpp:

#include <iostream>
#include <stdlib.h>

int main()
{
	setlocale(LC_CTYPE, "rus");
	int num;
	std::cout << "Введите любое целое число = ";
	std::cin >> num;
	std::cout << "Последняя цифра числа " << num << ": ";
	std::cout << num % 10 << std::endl;
	system("pause");
	return 0;
}
	/* Fri 25 Jan 2019 */
